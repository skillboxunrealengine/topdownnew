// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPS_Skillbox/PlayerStates.h"
#include "InventorySystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponID, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Count);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAmmoEmpty, int32, IndexSlot, bool, IsEmpty);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponUpdateSlots, int32, IndexSlotToChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponStartSwitchWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_SKILLBOX_API UInventorySystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventorySystemComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	FOnWeaponStartSwitchWeapon OnWeaponStartSwitchWeapon;
	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponStartSwitchWeapon_Delegate_Multicast();


	FOnSwitchWeapon OnSwitchWeapon;
	UFUNCTION(NetMulticast, Reliable)
	void OnSwitchWeapon_Delegate_Multicast(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewIndex);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory CPP///////////")
	FOnAmmoChange OnAmmoChange;
	UFUNCTION(NetMulticast, Reliable)
	void OnAmmoChange_Delegate_Multicast(EWeaponType TypeAmmo, int32 Count);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory CPP///////////")
	FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponAdditionalInfoChange_Delegate_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory CPP///////////")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponAmmoEmpty_Delegate_Multicast(int32 IndexSlot, bool IsEmpty);

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory CPP///////////")
	FOnWeaponUpdateSlots OnWeaponUpdateSlots;
	UFUNCTION(NetMulticast, Reliable)
	void OnWeaponUpdateSlots_Delegate_Multicast(int32 IndexSlotToChange, FWeaponSlot NewInfo);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WeaponSettingsCPP")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WeaponSettingsCPP")
	TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WeaponSettingsCPP")
	int32 MaxSlotsWeapon = 0;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WeaponSettingsCPP")
	int32 CurrentIndex;

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool SwitchWeaponToIndexRaw(int32 ChangeIndex, int32 OldIndex);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool SwitchWeaponToIndex(int32 ChangeIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool IsForward);
	int32 FindCorrectIndex(int32 NewIndex);
	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 IndexWeapon);
	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	void SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	void WeaponReloaded(EWeaponType TypeWeapon, int32 MinusCount);
	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool CheckAmmoForWeapon(int32 IndexWeapon, int32 &AvailableAmmo);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool CanTakeWeapon(int32 &FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool CanTakeAmmo(EWeaponType WeaponTypeAmmo);


	//UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	//FWeaponSlot SwitchWeaponToInventory(int32 ChangeIndex, FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool SwitchWeaponToInventory(int32 ChangeIndex, FWeaponSlot NewWeapon, FWeaponSlot& OldWeaponSlot);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	bool GetWeaponSlotFromInventoryWeapon(int32 ChangeIndex, FWeaponSlot& WeaponSlot);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	void UpdateWidgetsAmmo();

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "CPP FUNCTION")
	void TryGetWeaponToInventory_Server(AActor* PickupActor, FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	TArray<FWeaponSlot> GetWeaponSlots();

	UFUNCTION(BlueprintCallable, Category = "CPP FUNCTION")
	TArray<FAmmoSlot> GetAmmoSlots();

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "CPP FUNCTION")
	void InitInventory_Server(const TArray<FWeaponSlot> &NewWeaponSlots, const TArray<FAmmoSlot> &NewAmmoSlots, const int32 SelectedIndex);

	UFUNCTION(Server, Reliable)
	void CurrentIndex_Replicate_Server(int8 NewIndex);
};
