
#include "HealthSystemComponent.h"
#include "Net/UnrealNetwork.h"

UHealthSystemComponent::UHealthSystemComponent()
{

	PrimaryComponentTick.bCanEverTick = true;

}

void UHealthSystemComponent::OnDead_Delegate_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UHealthSystemComponent::OnHealthChange_Delegate_Multicast_Implementation(float tempMaxHealth, float tempHealth, float tempDamage, FVector tempHitLocation)
{
	OnHealthChange.Broadcast(tempMaxHealth, tempHealth, tempDamage, tempHitLocation);
}


void UHealthSystemComponent::BeginPlay()
{
	Super::BeginPlay();

	DamageCut = InitDamageCut;
	Health = MaxHealth;
	
}


void UHealthSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

float UHealthSystemComponent::GetMaxHealth()
{
	return MaxHealth;
}

float UHealthSystemComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthSystemComponent::SetCurrentHealth(float HealthValue)
{
	Health = HealthValue;
}

void UHealthSystemComponent::ChangeCurrentHealth(float HealthValue, bool IsPiercing, FVector HitLocation)
{
	if (IsAlive) {
		float CorrectHealth;
		if (IsPiercing) {
			CorrectHealth = HealthValue;
		}
		else {
			if (HealthValue > 0) {
				CorrectHealth = HealthValue;
			}
			else {
				CorrectHealth = HealthValue * DamageCut;
			}
		}
		Health = Health + CorrectHealth;

		OnHealthChange_Delegate_Multicast(MaxHealth, Health, CorrectHealth, HitLocation);

		if (Health > MaxHealth) {
			Health = MaxHealth;
		}
		else {
			if (Health <= 0) {
				OnDead_Delegate_Multicast();
				DeadEvent();
				IsAlive = false;
			}
		}
	}
}

void UHealthSystemComponent::DeadEvent_Implementation()
{
}


void UHealthSystemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthSystemComponent, MaxHealth);
	DOREPLIFETIME(UHealthSystemComponent, Health);
}