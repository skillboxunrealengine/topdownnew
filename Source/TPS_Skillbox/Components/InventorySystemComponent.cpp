#include "InventorySystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "TPS_Skillbox/Game/GameInstanceBase.h"

// Sets default values for this component's properties
UInventorySystemComponent::UInventorySystemComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UInventorySystemComponent::OnWeaponStartSwitchWeapon_Delegate_Multicast_Implementation()
{
	OnWeaponStartSwitchWeapon.Broadcast();
}

void UInventorySystemComponent::OnSwitchWeapon_Delegate_Multicast_Implementation(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewIndex)
{
	OnSwitchWeapon.Broadcast(WeaponID, WeaponAdditionalInfo, NewIndex);
}

void UInventorySystemComponent::OnAmmoChange_Delegate_Multicast_Implementation(EWeaponType TypeAmmo, int32 Count)
{
	OnAmmoChange.Broadcast(TypeAmmo, Count);
}

void UInventorySystemComponent::OnWeaponAdditionalInfoChange_Delegate_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UInventorySystemComponent::OnWeaponAmmoEmpty_Delegate_Multicast_Implementation(int32 IndexSlot, bool IsEmpty)
{
	OnWeaponAmmoEmpty.Broadcast(IndexSlot, IsEmpty);
}


void UInventorySystemComponent::OnWeaponUpdateSlots_Delegate_Multicast_Implementation(int32 IndexSlotToChange, FWeaponSlot NewInfo)
{
	OnWeaponUpdateSlots.Broadcast(IndexSlotToChange, NewInfo);
}

// Called when the game starts
void UInventorySystemComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UInventorySystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UInventorySystemComponent::SwitchWeaponToIndexRaw(int32 ChangeIndex, int32 OldIndex)
{
	bool IsSuccess = false;
	if (ChangeIndex == OldIndex) {
		IsSuccess = false;
	}
	else {
		FName NewIdWeapon;
		FAdditionalWeaponInfo NewAdditionalInfo;
		int32 NewCurrentIndex = 0;
		UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
		FWeaponInfo tempInfo;
		if (GI) {
			if (WeaponSlots.IsValidIndex(ChangeIndex)) {
				if (!WeaponSlots[ChangeIndex].NameItem.IsNone() && GI->GetWeaponInfoByName(WeaponSlots[ChangeIndex].NameItem, tempInfo)) {
					if (WeaponSlots[ChangeIndex].AdditionalInfo.Round > 0) {
						IsSuccess = true;
					}
					else {
						bool IsFind = false;
						int8 i = 0;
						while (i < AmmoSlots.Num() && !IsFind) {
							if (AmmoSlots[i].WeaponType == tempInfo.WeaponType && AmmoSlots[i].Count > 0) {
								IsSuccess = true;
								IsFind = true;
							}
							i++;
						}
					}
				}
				if (IsSuccess) {
					NewIdWeapon = WeaponSlots[ChangeIndex].NameItem;
					NewAdditionalInfo = WeaponSlots[ChangeIndex].AdditionalInfo;
					NewCurrentIndex = ChangeIndex;
				}
			}
		}


		if (IsSuccess) {
			OnSwitchWeapon_Delegate_Multicast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
			CurrentIndex_Replicate_Server(NewCurrentIndex);
		}
	}

	return IsSuccess;
}

bool UInventorySystemComponent::SwitchWeaponToIndex(int32 ChangeIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool IsForward)
{
	bool IsSuccess = false;
	int32 CorrectIndex = FindCorrectIndex(ChangeIndex);

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0; 
	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	FWeaponInfo uselesstempInfo;
	if (GI) {
		if (WeaponSlots.IsValidIndex(CorrectIndex)) {
			if (!WeaponSlots[CorrectIndex].NameItem.IsNone() && GI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, uselesstempInfo)) {
				if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0) {
					IsSuccess = true;
				}
				else {
					FWeaponInfo tempInfo;
					GI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, tempInfo);

					bool IsFind = false;
					int8 i = 0;
					while (i < AmmoSlots.Num() && !IsFind) {
						if (AmmoSlots[i].WeaponType == tempInfo.WeaponType && AmmoSlots[i].Count > 0) {
							IsSuccess = true;
							IsFind = true;
						}
						i++;
					}
				}
			}
			if (IsSuccess) {
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
				NewCurrentIndex = CorrectIndex;
			}
		}
	}

	if (!IsSuccess) {
		int8 iterq = 0;
		int8 iterw = 0;
		if (IsForward) {
			iterw = 0;
		}
		else {
			iterw = WeaponSlots.Num() - 1;
		}

		while (iterq < WeaponSlots.Num() && !IsSuccess) {
			iterq++;
			int8 tempindex;
			if (IsForward) {
				tempindex = ChangeIndex + iterq;
				tempindex = FindCorrectIndex(tempindex);
			}
			else {
				tempindex = ChangeIndex - iterq;
				tempindex = FindCorrectIndex(tempindex);
				UE_LOG(LogTemp, Error, TEXT("NewIndex: %d"), tempindex);
			}
			if (WeaponSlots.IsValidIndex(tempindex)) {
				if (!WeaponSlots[tempindex].NameItem.IsNone() && GI->GetWeaponInfoByName(WeaponSlots[tempindex].NameItem, uselesstempInfo)) {
					if (WeaponSlots[tempindex].AdditionalInfo.Round > 0) {
						IsSuccess = true;
						NewIdWeapon = WeaponSlots[tempindex].NameItem;
						NewAdditionalInfo = WeaponSlots[tempindex].AdditionalInfo;
						NewCurrentIndex = tempindex;
					}
					else {
						FWeaponInfo tempInfo;
						GI->GetWeaponInfoByName(WeaponSlots[tempindex].NameItem, tempInfo);

						bool IsFind = false;
						int8 i = 0;
						while (i < AmmoSlots.Num() && !IsFind) {
							if (AmmoSlots[i].WeaponType == tempInfo.WeaponType && AmmoSlots[i].Count > 0) {
								IsSuccess = true;
								NewIdWeapon = WeaponSlots[tempindex].NameItem;
								NewAdditionalInfo = WeaponSlots[tempindex].AdditionalInfo;
								NewCurrentIndex = tempindex;
								IsFind = true;
							}
							i++;
						}
					}
				}
				else {
					if (OldIndex != FindCorrectIndex(iterw)) {
						if (WeaponSlots.IsValidIndex(iterw))
						{
							if (!WeaponSlots[iterw].NameItem.IsNone() && GI->GetWeaponInfoByName(WeaponSlots[iterw].NameItem, uselesstempInfo))
							{
								if (WeaponSlots[iterw].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									IsSuccess = true;
									NewIdWeapon = WeaponSlots[iterw].NameItem;
									NewAdditionalInfo = WeaponSlots[iterw].AdditionalInfo;
									NewCurrentIndex = iterw;
								}
								else
								{
									FWeaponInfo myInfo;

									GI->GetWeaponInfoByName(WeaponSlots[iterw].NameItem, myInfo);

									bool IsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !IsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
										{
											//WeaponGood
											IsSuccess = true;
											NewIdWeapon = WeaponSlots[iterw].NameItem;
											NewAdditionalInfo = WeaponSlots[iterw].AdditionalInfo;
											NewCurrentIndex = iterw;
											IsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlots.IsValidIndex(iterw))
						{
							if (!WeaponSlots[iterw].NameItem.IsNone() && GI->GetWeaponInfoByName(WeaponSlots[iterw].NameItem, uselesstempInfo))
							{
								if (WeaponSlots[iterw].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;

									GI->GetWeaponInfoByName(WeaponSlots[iterw].NameItem, myInfo);

									bool IsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !IsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Count > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with amm need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					if (IsForward) {
						iterw++;
					}
					else {
						iterw--;
					}
				}
			}
		}
	}


	//int8 i = 0;
	//while (i < WeaponSlots.Num() && !IsSuccess) {
	//	if (i == CorrectIndex) {
	//		if (!WeaponSlots[i].NameItem.IsNone()) {
	//			NewIdWeapon = WeaponSlots[i].NameItem;
	//			NewAdditionalInfo = WeaponSlots[i].AdditionalInfo;
	//			IsSuccess = true;
	//		}
	//	}
	//	i++;
	//}

	if (IsSuccess) {
		SetAdditionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon_Delegate_Multicast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}

	CurrentIndex_Replicate_Server(NewCurrentIndex);

	return IsSuccess;
}

int32 UInventorySystemComponent::FindCorrectIndex(int32 NewIndex)
{
	int32 CorrectIndex = NewIndex;
	if (NewIndex > WeaponSlots.Num() - 1) {
		NewIndex = NewIndex % (WeaponSlots.Num());
		CorrectIndex = NewIndex;
	}
	else {
		if (NewIndex < 0) {
			NewIndex = WeaponSlots.Num() - (-NewIndex % WeaponSlots.Num());
			CorrectIndex = NewIndex;
		}
	}
	return CorrectIndex;
}

FAdditionalWeaponInfo UInventorySystemComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool IsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !IsFind) {
			if (i == IndexWeapon) {
				result = WeaponSlots[i].AdditionalInfo;
				IsFind = true;
			}
			i++;
		}
		if (!IsFind) {
			UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeaponInfo NOT FOUND WEAPON WITH INDEX: %d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeaponInfo NOT FOUND/NOT CORRECT INDEX WEAPON: %d"), IndexWeapon)
	}
	return result;
}

int32 UInventorySystemComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool IsFind = false;
	while (i < WeaponSlots.Num() && !IsFind) {
		if (WeaponSlots[i].NameItem == IdWeaponName) {
			IsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

void UInventorySystemComponent::SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool IsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !IsFind) {
			if (i == IndexWeapon) {
				WeaponSlots[i].AdditionalInfo = NewInfo;
				IsFind = true;

				OnWeaponAdditionalInfoChange_Delegate_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!IsFind) {
			UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeaponInfo NOT FOUND WEAPON WITH INDEX: %d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeaponInfo NOT FOUND/NOT CORRECT INDEX WEAPON: %d"), IndexWeapon);
	}
}

void UInventorySystemComponent::WeaponReloaded(EWeaponType TypeWeapon, int32 MinusCount)
{
	bool IsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !IsFind) {
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			AmmoSlots[i].Count += MinusCount;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount) {
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
			}
			OnAmmoChange_Delegate_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);
			IsFind = true;
		}
		i++;
	}
}

bool UInventorySystemComponent::CheckAmmoForWeapon(int32 IndexWeapon, int32 &AvailableAmmo)
{
	AvailableAmmo = 0;

	bool IsAmmoNotEmpty = false;
	bool IsFind = false;
	int8 i = 0;
	
	while (i < AmmoSlots.Num() && !IsFind) {
		if (AmmoSlots[i].WeaponType == WeaponSlots[IndexWeapon].WeaponType) {
			IsFind = true;
			if (AmmoSlots[i].Count > 0) {
				IsAmmoNotEmpty = true;
				AvailableAmmo = AmmoSlots[i].Count;
			}
		}
		i++;
	}
	return IsAmmoNotEmpty;
}

bool UInventorySystemComponent::CanTakeWeapon(int32 &FreeSlot)
{
	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	FWeaponInfo tempWeaponInfo;

	bool IsFind = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !IsFind) {
		if (!GI->GetWeaponInfoByName(WeaponSlots[i].NameItem, tempWeaponInfo) || WeaponSlots[i].NameItem.IsNone()) {
			IsFind = true;
			FreeSlot = i;
		}

		i++;
	}
	return IsFind;
}

bool UInventorySystemComponent::CanTakeAmmo(EWeaponType WeaponTypeAmmo)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result) {
		if (AmmoSlots[i].WeaponType == WeaponTypeAmmo && AmmoSlots[i].Count < AmmoSlots[i].MaxCount) {
			result = true;
		}
		i++;
	}
	return result;
}

//FWeaponSlot UInventorySystemComponent::SwitchWeaponToInventory(int32 ChangeIndex, FWeaponSlot NewWeapon)
//{
//	FWeaponSlot result;
//	if (WeaponSlots.IsValidIndex(ChangeIndex)) {
//		result = WeaponSlots[ChangeIndex];
//		WeaponSlots[ChangeIndex] = NewWeapon;
//		if (CurrentIndex == ChangeIndex) {
//			OnSwitchWeapon.Broadcast(WeaponSlots[CurrentIndex].NameItem, WeaponSlots[CurrentIndex].AdditionalInfo, CurrentIndex);
//		}
//	}
//	
//	OnWeaponUpdateSlots.Broadcast(ChangeIndex, NewWeapon);
//	return result;
//}

bool UInventorySystemComponent::SwitchWeaponToInventory(int32 ChangeIndex, FWeaponSlot NewWeapon, FWeaponSlot &OldWeaponSlot)
{
	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	FWeaponInfo tempWeaponInfo;
	bool result = false;
	if (GI->GetWeaponInfoByName(NewWeapon.NameItem, tempWeaponInfo) && !NewWeapon.NameItem.IsNone()) {
		if (WeaponSlots.IsValidIndex(ChangeIndex) && GetWeaponSlotFromInventoryWeapon(ChangeIndex, OldWeaponSlot)) {
			WeaponSlots[ChangeIndex] = NewWeapon;
			if (CurrentIndex == ChangeIndex) {
				OnSwitchWeapon_Delegate_Multicast(WeaponSlots[CurrentIndex].NameItem, WeaponSlots[CurrentIndex].AdditionalInfo, CurrentIndex);
			}
		}
		result = true;
		OnWeaponUpdateSlots_Delegate_Multicast(ChangeIndex, NewWeapon);
	}
	
	return result;
}

bool UInventorySystemComponent::GetWeaponSlotFromInventoryWeapon(int32 ChangeIndex, FWeaponSlot &WeaponSlot)
{
	bool result = false;
	FDropItem tempdropitem;

	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());

	if (WeaponSlots.IsValidIndex(ChangeIndex)) {
		result = GI->GetDropWeaponSlotByName(WeaponSlots[ChangeIndex].NameItem, tempdropitem);
		WeaponSlot = tempdropitem.WeaponInfo;
		WeaponSlot.AdditionalInfo = WeaponSlots[ChangeIndex].AdditionalInfo;
	}
	return result;
}

void UInventorySystemComponent::UpdateWidgetsAmmo()
{
	int8 i = 0;
	while (i < WeaponSlots.Num()) {
		bool IsMagazineEmpty = true;
		if (WeaponSlots[i].AdditionalInfo.Round > 0) {
			IsMagazineEmpty = false;
		}
		bool result = false;
		bool IsFind = false;
		int8 j = 0;
		while (j < AmmoSlots.Num() && !IsFind) {
			bool IsAmmoEmpty = true;
			if (WeaponSlots[i].WeaponType == AmmoSlots[j].WeaponType) {
				if (AmmoSlots[j].Count > 0) {
					IsAmmoEmpty = false;
				}
				if (IsAmmoEmpty && IsMagazineEmpty) {
					result = true;
					IsFind = true;
				}
				OnWeaponAmmoEmpty_Delegate_Multicast(i, result);
			}
			j++;
		}
		i++;
	}
}

void UInventorySystemComponent::TryGetWeaponToInventory_Server_Implementation(AActor* PickupActor, FWeaponSlot NewWeapon)
{
	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetWorld()->GetGameInstance());
	FWeaponInfo tempWeaponInfo;
	bool IsSuccess = false;
	int32 IndexSlot = -1;
	if (GI->GetWeaponInfoByName(NewWeapon.NameItem, tempWeaponInfo) && !NewWeapon.NameItem.IsNone()) {
		if (CanTakeWeapon(IndexSlot)) {
			if (WeaponSlots.IsValidIndex(IndexSlot)) {
				WeaponSlots[IndexSlot] = NewWeapon;
				OnWeaponUpdateSlots_Delegate_Multicast(IndexSlot, NewWeapon);
				//OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, NewWeapon.AdditionalInfo);
				if (PickupActor) {
					PickupActor->Destroy();
				}
				IsSuccess = true;
			}
		}
		else {
			OnWeaponStartSwitchWeapon_Delegate_Multicast();
		}
	}
}

TArray<FWeaponSlot> UInventorySystemComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UInventorySystemComponent::GetAmmoSlots()
{
	return AmmoSlots;
}

void UInventorySystemComponent::CurrentIndex_Replicate_Server_Implementation(int8 NewIndex)
{
	CurrentIndex = NewIndex;
}

void UInventorySystemComponent::InitInventory_Server_Implementation(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots, const int32 SelectedIndex)
{
	if (!NewWeaponSlots.IsEmpty()) {
		WeaponSlots = NewWeaponSlots;
	}
	if (!NewAmmoSlots.IsEmpty()) {
		AmmoSlots = NewAmmoSlots;
	}
	if (WeaponSlots.IsValidIndex(SelectedIndex)) {
		SwitchWeaponToIndexRaw(SelectedIndex, -1);
	}
	else if (WeaponSlots.IsValidIndex(0)) {
		SwitchWeaponToIndexRaw(0, -1);
	}

	MaxSlotsWeapon = WeaponSlots.Num();
	

}

void UInventorySystemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventorySystemComponent, WeaponSlots);
	DOREPLIFETIME(UInventorySystemComponent, AmmoSlots);
	DOREPLIFETIME(UInventorySystemComponent, MaxSlotsWeapon);
	DOREPLIFETIME(UInventorySystemComponent, CurrentIndex);
}