// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SkillboxCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "TPS_Skillbox/Game/GameInstanceBase.h"
#include "TPS_Skillbox/Components/InventorySystemComponent.h"
#include "TPS_Skillbox/Components/CharacterHealthSystemComponent.h"
#include "Materials/Material.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"

ATPS_SkillboxCharacter::ATPS_SkillboxCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 400.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = false;
	GetCharacterMovement()->bSnapToPlaneAtStart = false;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->SetRelativeRotation(FRotator(-55.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = true; // Don't want to pull camera in when it collides with level
	CameraBoom->TargetArmLength = 1500.0f;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventorySystemComponent = CreateDefaultSubobject<UInventorySystemComponent>(TEXT("InventoryComponent"));

	HealthSystemComponent = CreateDefaultSubobject<UCharacterHealthSystemComponent>(TEXT("HealthComponent"));

	if (HealthSystemComponent) {
		HealthSystemComponent->OnDead.AddDynamic(this, &ATPS_SkillboxCharacter::PlayerDeath);
	}

	if (InventorySystemComponent) {
		InventorySystemComponent->OnSwitchWeapon.AddDynamic(this, &ATPS_SkillboxCharacter::InitWeapon_Server);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	bReplicates = true;
}

void ATPS_SkillboxCharacter::BeginPlay()
{
	Super::BeginPlay();
	ChangeMovementState(EMovementState::Walk_state);
	GetCharacterMovement()->MaxWalkSpeed = MovementInfo.WalkSpeed;
	IsAlive = true;

	GI = Cast<UGameInstanceBase>(GetGameInstance());
	
	if (!PlayerController) {
		PlayerController = Cast<APlayerController>(GetController());
		if (PlayerController)
		{
			if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
			{
				Subsystem->AddMappingContext(DefaultMappingContext, 0);
			}
		}
	}

	FActorSpawnParameters Params;
	CursorPlate = GetWorld()->SpawnActor<AActor>(ObjectCursor, FTransform(FRotator::ZeroRotator, FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 500.0f), FVector(1.0f)), Params);
}

void ATPS_SkillboxCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (!PlayerController) {
		PlayerController = Cast<APlayerController>(NewController);
		if (PlayerController)
		{
			if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
			{
				Subsystem->AddMappingContext(DefaultMappingContext, 0);
			}
		}
	}
}

void ATPS_SkillboxCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (Controller && Controller->IsLocalPlayerController()) {
		StaminaTick(DeltaSeconds);
		LookTick(DeltaSeconds);
	}
}

void ATPS_SkillboxCharacter::LookTick(float DeltaTime)
{
	if (IsPlayerAlive()) {
		FHitResult Hit;
		if (PlayerController) {
			if (PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel2, true, Hit)) {
				if (CurrentWeapon) {
					ShootEndLocation = Hit.Location + Displacement;
					if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f)) {
						ShouldReduceDispersion = true;
					}
					else {
						ShouldReduceDispersion = false;
					}
					CurrentWeapon->ChangeWeaponState_Server(ShootEndLocation, ShouldReduceDispersion);
				}
				LookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Hit.Location);
			}
			if (OrientToMovement && !FMath::IsNearlyZero(GetVelocity().Size(), 0.5f)) {
				RotateTo = GetVelocity().Rotation().Yaw;
				if (!RotationLerp) {
					RotationLerp = true;
				}
				RotateFrom = GetActorRotation();
			}
			else {
				if (RotationLerp) {
					RotateTo = FRotator(UKismetMathLibrary::RLerp(RotateFrom, LookRotation, RotationTimer / RotationTime, true)).Yaw;
					RotationTimer += DeltaTime;
					if (RotationTimer >= RotationTime) {
						RotationLerp = false;
						RotationTimer = 0;
					}
				}
				else {
					RotateTo = LookRotation.Yaw;
				}
			}
			SetActorRotation(FQuat(FRotator(0.0f, RotateTo, 0.0f)));
			SetActorRotation_OnServer(RotateTo);
		}
	}
}

void ATPS_SkillboxCharacter::StaminaTick(float DeltaTime)
{
	if (Controller && Controller->IsLocalPlayerController()) {
		if (MovementState == EMovementState::Run_state && Stamina > 0 && !GetVelocity().IsNearlyZero(300.0f)) {
			Stamina = Stamina - (StaminaRegenerate * DeltaTime);
			if (Stamina <= 0) {
				ChangeMovementState(EMovementState::Walk_state);
				Stamina = 0.0f;
			}
			TimerRegenerateStamina = TimeToRegenerateStamina;
		}
		else {
			if (Stamina <= MaxStamina) {
				if (TimerRegenerateStamina <= 0) {
					Stamina = Stamina + (StaminaRegenerate * DeltaTime);
					if (Stamina >= MaxStamina) {
						Stamina = MaxStamina;
					}
				}
				else {
					TimerRegenerateStamina = TimerRegenerateStamina - DeltaTime;
				}
			}
		}
	}
}

void ATPS_SkillboxCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	if (EnhancedInputComponent)
	{
		// ������� ������� ������������
		EnhancedInputComponent->BindAction(SetMovementAction, ETriggerEvent::Triggered, this, &ATPS_SkillboxCharacter::OnMovementTriggered);

		// ������� ������� ����
		EnhancedInputComponent->BindAction(SetRunAction, ETriggerEvent::Started, this, &ATPS_SkillboxCharacter::OnRunStarted);
		EnhancedInputComponent->BindAction(SetRunAction, ETriggerEvent::Completed, this, &ATPS_SkillboxCharacter::OnRunCompleted);

		// ������� ������� ������������
		EnhancedInputComponent->BindAction(SetAimAction, ETriggerEvent::Started, this, &ATPS_SkillboxCharacter::OnAimStarted);
		EnhancedInputComponent->BindAction(SetAimAction, ETriggerEvent::Completed, this, &ATPS_SkillboxCharacter::OnAimCompleted);

		// ������� ������� ��������
		EnhancedInputComponent->BindAction(SetAttackAction, ETriggerEvent::Started, this, &ATPS_SkillboxCharacter::OnAttackStarted);
		EnhancedInputComponent->BindAction(SetAttackAction, ETriggerEvent::Completed, this, &ATPS_SkillboxCharacter::OnAttackCompleted);

		// ������� ����� ������
		EnhancedInputComponent->BindAction(SetSwitchWeaponAction, ETriggerEvent::Started, this, &ATPS_SkillboxCharacter::OnSwitchWeaponStarted);

		// ������� ������� �����������
		EnhancedInputComponent->BindAction(SetReloadAction, ETriggerEvent::Started, this, &ATPS_SkillboxCharacter::TryReloadStarted);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ATPS_SkillboxCharacter::ChangeMovementState(EMovementState State)
{
	if (GetController() && GetController()->IsLocalPlayerController()) {
		if (State == EMovementState::Run_state && Stamina > 0) {
			SetMovementState_OnServer(State);
		}
		else if (State != EMovementState::Run_state) {
			SetMovementState_OnServer(State);
		}
	}
}

void ATPS_SkillboxCharacter::ChangeMovementSpeed()
{
	float Speed = MovementInfo.WalkSpeed;
	switch (MovementState) {
	case EMovementState::Walk_state:
		Speed = MovementInfo.WalkSpeed;
		OrientToMovement = false;
		break;
	case EMovementState::Run_state:
		Speed = MovementInfo.RunSpeed;
		OrientToMovement = true;
		break;
	case EMovementState::Aim_state:
		Speed = MovementInfo.AimSpeed;
		OrientToMovement = false;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = Speed;

	if (GetCurrentWeapon())
	{
		GetCurrentWeapon()->UpdateStateWeapon_Server(MovementState);
	}

}

AWeaponDefaults* ATPS_SkillboxCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPS_SkillboxCharacter::InitWeapon_Server_Implementation(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 Index)
{
	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon->CancelReloading_Server();
		CurrentWeapon = nullptr;
	}
	GI = Cast<UGameInstanceBase>(GetGameInstance());

	FWeaponInfo WeaponInfo;
	if (GI)
	{
		if (GI->GetWeaponInfoByName(WeaponID, WeaponInfo))
		{
			if (WeaponInfo.WeaponClass) {
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = this;

				AWeaponDefaults* myWeapon = Cast<AWeaponDefaults>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
					CurrentWeapon = myWeapon;

					CurrentWeapon->WeaponSetting = WeaponInfo;
					CurrentWeapon->ReloadTimer = WeaponInfo.ReloadTime;

					CurrentWeapon->UpdateStateWeapon_Server(MovementState);

					CurrentWeapon->AddWeaponSetting = WeaponAdditionalInfo;

					CurrentIndexWeapon = Index;
					

					CurrentWeapon->OnWeaponFireStart.AddDynamic(this, &ATPS_SkillboxCharacter::WeaponFireStarted);
					CurrentWeapon->OnWeaponFireEnd.AddDynamic(this, &ATPS_SkillboxCharacter::WeaponFireEnded);
					CurrentWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPS_SkillboxCharacter::WeaponReloadStarted);
					CurrentWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPS_SkillboxCharacter::WeaponReloadEnded);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanWeaponReload()) {
						CurrentWeapon->WeaponReloadStart_Server();
					}
				}
			}
		}
	}
}
void ATPS_SkillboxCharacter::TryReload_Server_Implementation()
{
	if (CurrentWeapon) {
		if (!CurrentWeapon->IsReloading) {
			if (CurrentWeapon->GetWeaponRound() != CurrentWeapon->WeaponSetting.MaxRounds && CurrentWeapon->CanWeaponReload()) {
				CurrentWeapon->WeaponReloadStart_Server();
			}
		}
	}
}

void ATPS_SkillboxCharacter::PlayerDeath()
{
	if (IsAlive) {
		float AnimTime = 0.0f;
		int8 i = FMath::RandRange(0, DeathMontages.Num() - 1);
		if (DeathMontages.IsValidIndex(i) && DeathMontages[i] && GetMesh()) {

			AnimTime = DeathMontages[i]->GetPlayLength() - 0.1f;
			PlayMontage_Multicast(DeathMontages[i]);
		}
		
		if (CurrentWeapon) {
			CurrentWeapon->SetWeaponStateFire_Server(false);
		}
		IsAlive = false;

		PlayerDeath_BP();

		
		//if (GetController()) {
		//	GetController()->UnPossess();
		//}

		if (Controller && Controller->IsLocalPlayerController()) {
			CursorPlate->Destroy();
		}

		UnPossessed();

		GetWorldTimerManager().SetTimer(TimerHande_TimeToRagdoll, this, &ATPS_SkillboxCharacter::EnableRagdoll_Multicast, AnimTime, false);
	}
}

void ATPS_SkillboxCharacter::PlayerDeath_BP_Implementation()
{
	// In BP
}

void ATPS_SkillboxCharacter::WeaponFireStarted(UAnimMontage* AnimFire, UAnimMontage* AnimAimFire)
{
	if (InventorySystemComponent && CurrentWeapon) {
		InventorySystemComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AddWeaponSetting);
	}

	WeaponFireStarted_BP(AnimFire, AnimAimFire);
}

void ATPS_SkillboxCharacter::WeaponFireEnded()
{
	WeaponFireEnded_BP();
}

void ATPS_SkillboxCharacter::WeaponReloadStarted(UAnimMontage* Anim)
{
	WeaponReloadStarted_BP(Anim);
}

void ATPS_SkillboxCharacter::WeaponReloadEnded(bool IsSuccess, int32 KeepAmmo)
{
	if (InventorySystemComponent && CurrentWeapon) {
		InventorySystemComponent->WeaponReloaded(CurrentWeapon->WeaponSetting.WeaponType, KeepAmmo);
		InventorySystemComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->AddWeaponSetting);
	}
	WeaponReloadEnded_BP(IsSuccess);
}

void ATPS_SkillboxCharacter::TrySwitchNextWeapon()
{
	if (InventorySystemComponent->WeaponSlots.Num() > 1)
	{

		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon) {
			OldInfo = CurrentWeapon->AddWeaponSetting;
			if (CurrentWeapon->IsReloading) {
				CurrentWeapon->CancelReloading_Server();
			}
		}

		if (InventorySystemComponent)
		{
			if (InventorySystemComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true)) {

			}
		}
	}
}

void ATPS_SkillboxCharacter::TrySwitchPreviousWeapon()
{
	if (InventorySystemComponent->WeaponSlots.Num() > 1)
	{

		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon) {
			OldInfo = CurrentWeapon->AddWeaponSetting;
			if (CurrentWeapon->IsReloading) {
				CurrentWeapon->CancelReloading_Server();
			}
		}

		if (InventorySystemComponent)
		{
			if (InventorySystemComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false)) {

			}
		}
	}
}

float ATPS_SkillboxCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	HealthSystemComponent->ChangeCurrentHealth(-ActualDamage);
	return ActualDamage;
}

bool ATPS_SkillboxCharacter::IsPlayerAlive()
{
	return IsAlive;
}

void ATPS_SkillboxCharacter::EnableRagdoll_Multicast_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("RAGDOLL PROCESSED!!!!!"));
	if (GetMesh()) {
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ATPS_SkillboxCharacter::PlayMontage_Multicast_Implementation(UAnimMontage* MontageToPlay)
{
	GetMesh()->GetAnimInstance()->Montage_Play(MontageToPlay);
}

EPhysicalSurface ATPS_SkillboxCharacter::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType1;
	if (HealthSystemComponent->GetCurrentShieldCapacity() <= 0) {
		if (GetMesh()) {
			UMaterialInterface* Material = GetMesh()->GetMaterial(0);
			if (Material) {
				result = Material->GetPhysicalMaterial()->SurfaceType;
			}
		}
	}
	return result;
}

TArray<UStateEffectBase*> ATPS_SkillboxCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_SkillboxCharacter::RemoveEffect(UStateEffectBase* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATPS_SkillboxCharacter::AddEffect(UStateEffectBase* NewEffect)
{
	Effects.Add(NewEffect);
}

void ATPS_SkillboxCharacter::SetActorRotation_OnServer_Implementation(float Yaw)
{
	SetActorRotation_Multicast(Yaw);
}

void ATPS_SkillboxCharacter::SetActorRotation_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalController()) {
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPS_SkillboxCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATPS_SkillboxCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	ChangeMovementSpeed();
}

void ATPS_SkillboxCharacter::WeaponFireStarted_BP_Implementation(UAnimMontage* AnimFire, UAnimMontage* AnimAimFire)
{
	// IN BP
}

void ATPS_SkillboxCharacter::WeaponFireEnded_BP_Implementation()
{
	// IN BP
}

void ATPS_SkillboxCharacter::WeaponReloadStarted_BP_Implementation(UAnimMontage* Anim)
{
	// IN BP
}

void ATPS_SkillboxCharacter::WeaponReloadEnded_BP_Implementation(bool IsSuccess)
{
	// IN BP
}


// ���������� // ------------------------------------------------------------------------------
void ATPS_SkillboxCharacter::OnMovementTriggered(const FInputActionValue& Value)
{
	if (Controller && IsPlayerAlive()) {
		if (Value.Get<FVector2D>().X != 0 || Value.Get<FVector2D>().Y != 0) {
			AddMovementInput(FVector((1 * Value.Get<FVector2D>().X), (1 * Value.Get<FVector2D>().Y), 0), 1);
		}
	}
}

void ATPS_SkillboxCharacter::OnRunStarted()
{
	if (IsPlayerAlive()) {
		if (IsAimPressed == false) {
			ChangeMovementState(EMovementState::Run_state);
		}

		IsRunPressed = true;
	}
}

void ATPS_SkillboxCharacter::OnRunCompleted()
{
	if (IsPlayerAlive()) {
		if (IsAimPressed == false) {
			ChangeMovementState(EMovementState::Walk_state);
		}
		IsRunPressed = false;
	}
}

void ATPS_SkillboxCharacter::OnAimStarted()
{
	if (IsPlayerAlive()) {
		ChangeMovementState(EMovementState::Aim_state);
		IsAimPressed = true;
	}
}

void ATPS_SkillboxCharacter::OnAimCompleted()
{
	if (IsPlayerAlive()) {
		if (IsRunPressed == true) {
			ChangeMovementState(EMovementState::Run_state);
		}
		else {
			ChangeMovementState(EMovementState::Walk_state);
		}
		IsAimPressed = false;
	}
}

void ATPS_SkillboxCharacter::OnAttackStarted()
{
	if (IsPlayerAlive()) {
		DoAttack_Server(true);
	}
}

void ATPS_SkillboxCharacter::OnAttackCompleted()
{
	if (IsPlayerAlive()) {
		DoAttack_Server(false);
	}
}

void ATPS_SkillboxCharacter::DoAttack_Server_Implementation(bool IsFiring)
{
	if (CurrentWeapon) {
		CurrentWeapon->SetWeaponStateFire_Server(IsFiring);
	}
}

void ATPS_SkillboxCharacter::TryReloadStarted()
{
	if (IsPlayerAlive()) {
		TryReload_Server();
	}
}

void ATPS_SkillboxCharacter::OnSwitchWeaponStarted(const FInputActionValue& Value)
{
	if (IsPlayerAlive()) {
		float SwitchValue = Value.Get<float>();
		if (SwitchValue < 0) {
			TrySwitchNextWeapon();
		}
		else if (SwitchValue > 0) {
			TrySwitchPreviousWeapon();
		}
	}
}

void ATPS_SkillboxCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const 
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_SkillboxCharacter, MovementState);
	DOREPLIFETIME(ATPS_SkillboxCharacter, CurrentWeapon);
	DOREPLIFETIME(ATPS_SkillboxCharacter, IsAlive);
	DOREPLIFETIME(ATPS_SkillboxCharacter, CurrentIndexWeapon);
}