// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_Skillbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_Skillbox, "TPS_Skillbox" );

DEFINE_LOG_CATEGORY(LogTPS_Skillbox)
DEFINE_LOG_CATEGORY(LogNetwork)