#include "WeaponDefaults.h"
#include "TPS_Skillbox/Effects/StateEffectBase.h"
#include "Engine/StaticMeshActor.h"
#include <Kismet/GameplayStatics.h>
#include "Kismet/KismetMathLibrary.h"
#include "Components/DecalComponent.h"
#include "Net/UnrealNetwork.h"
#include "TPS_Skillbox/Components/InventorySystemComponent.h"

// Sets default values
AWeaponDefaults::AWeaponDefaults()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	// �������� �����
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// �������� ���� ������ �� ��������
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// �������� ���� ��� �������, ���� ���� �� ��������
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	// �������������� ����
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	// �������������� ������ ��������
	SleeveLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveLocation"));
	SleeveLocation->SetupAttachment(RootComponent);

	// �������������� ������ ��������
	MagazineLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MagazineLocation"));
	MagazineLocation->SetupAttachment(RootComponent);
}

void AWeaponDefaults::OnWeaponFireStart_Delegate_Multicast_Implementation(UAnimMontage* tempAnimFire, UAnimMontage* tempAnimAimFire)
{
	OnWeaponFireStart.Broadcast(tempAnimFire, tempAnimAimFire);
}

void AWeaponDefaults::OnWeaponFireEnd_Delegate_Multicast_Implementation()
{
	OnWeaponFireEnd.Broadcast();
}

void AWeaponDefaults::OnWeaponReloadStart_Delegate_Multicast_Implementation(UAnimMontage* tempAnim)
{
	OnWeaponReloadStart.Broadcast(tempAnim);
}

void AWeaponDefaults::OnWeaponReloadEnd_Delegate_Multicast_Implementation(bool tempIsSuccess, int32 tempKeepAmmo)
{
	OnWeaponReloadEnd.Broadcast(tempIsSuccess, tempKeepAmmo);
}

// Called when the game starts or when spawned
void AWeaponDefaults::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWeaponDefaults::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellTick(DeltaTime);
	MagazineTick(DeltaTime);
}

void AWeaponDefaults::FireTick(float DeltaTime)
{
	if (WeaponFiring) {
		if (FireTimer < 0.0f && CheckWeaponCanFire()) {
			Fire();
		}
	}
	if (FireTimer >= 0.0f) {
		FireTimer -= DeltaTime;
	}
}

void AWeaponDefaults::ReloadTick(float DeltaTime)
{
	if (IsReloading) {
		if (ReloadTimer <= 0.0f) {
			WeaponReloadEnd_Server();
		}
		else {
			ReloadTimer -= DeltaTime;
		}
		if (DoOnceReloadEndSound && ReloadTimer <= 1) {
			DoOnceReloadEndSound = false;
			SpawnSound_Multicast(WeaponSetting.ReloadWeaponEnd, ShootLocation->GetComponentLocation());
		}
	}
}

void AWeaponDefaults::ShellTick(float DeltaTime)
{
	if (DropShellFlag) {
		if (DropShellTimer < 0.0f) {
			DropShellFlag = false;
			InitDropMesh_Server(WeaponSetting.ShellBulletMesh.DropMesh, SleeveLocation, WeaponSetting.ShellBulletMesh.DropMeshLifeTime, WeaponSetting.ShellBulletMesh.DropMeshImpulse, WeaponSetting.ShellBulletMesh.DropMeshImpulseDispersion, WeaponSetting.ShellBulletMesh.DropMeshMass);
		}
		else {
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefaults::MagazineTick(float DeltaTime)
{
	if (DropClipFlag) {
		if (DropClipTimer < 0.0f) {
			DropClipFlag = false;
			InitDropMesh_Server(WeaponSetting.ClipDropMesh.DropMesh, MagazineLocation, WeaponSetting.ClipDropMesh.DropMeshLifeTime, WeaponSetting.ClipDropMesh.DropMeshImpulse, WeaponSetting.ClipDropMesh.DropMeshImpulseDispersion, WeaponSetting.ClipDropMesh.DropMeshMass);
		}
		else {
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponDefaults::WeaponInit(FWeaponInfo WeaponInfo)
{
	// �������� �� ��, ���� �� ��� �� ��������, ���� �� ����������� -> �������
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->GetSkeletalMeshAsset())
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	// ���� �� �� ��� ��������, �� ���� ��� �� �������� ����, �� ���������� ���� -> ���� :)
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	WeaponSetting = WeaponInfo;
	AddWeaponSetting.Round = WeaponSetting.MaxRounds;
}



void AWeaponDefaults::InitDropMesh_Server_Implementation(UStaticMesh* Mesh, UArrowComponent* DropTransform, float LifeTime, float ImpulseStrength, float ImpulseDispersion, float MeshMass)
{
	if (Mesh) {
		FTransform Transform = DropTransform->GetComponentTransform();

		InitDropMesh_Multicast(Mesh, DropTransform, Transform, LifeTime, ImpulseStrength, ImpulseDispersion, MeshMass);
	}
}

void AWeaponDefaults::InitDropMesh_Multicast_Implementation(UStaticMesh* Mesh, UArrowComponent* DropTransform, FTransform Transform, float LifeTime, float ImpulseStrength, float ImpulseDispersion, float MeshMass)
{
	AStaticMeshActor* DropActor = nullptr;

	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Params.Owner = this;
	DropActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Params);

	if (DropActor && DropActor->GetStaticMeshComponent()) {
		DropActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		DropActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		DropActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;

		DropActor->SetLifeSpan(LifeTime);
		if (MeshMass > 0.0f) {
			DropActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, MeshMass, true);
		}

		DropActor->GetStaticMeshComponent()->SetSimulatePhysics(true);

		DropActor->GetStaticMeshComponent()->SetStaticMesh(Mesh);

		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		DropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);


		DropActor->SetActorTickEnabled(true);
		DropActor->SetReplicates(true);

		FVector FinalDir;
		FVector LocalDir = DropTransform->GetComponentRotation().Vector();
		if (!FMath::IsNearlyZero(ImpulseDispersion)) {
			FinalDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseDispersion);
		}
		FinalDir.GetSafeNormal(0.0001f);
		DropActor->GetStaticMeshComponent()->AddImpulse(FinalDir * ImpulseStrength);
	}
}

void AWeaponDefaults::SetWeaponStateFire_Server_Implementation(bool IsFire)
{
	WeaponFiring = IsFire;
}

bool AWeaponDefaults::CheckWeaponCanFire()
{
	bool result = false;
	if (CanFire && AddWeaponSetting.Round > 0 && !IsReloading) {
		result = CanFire;
	}
	else {
		result = false;
	}
	return result;
}

FProjectileInfo AWeaponDefaults::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}


void AWeaponDefaults::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	AddWeaponSetting.Round -= 1;
	ChangeDispersionByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		//FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < GetNumberProjectileByShot(); i++) {
			EndLocation = GetFireEndLocation();

			FVector Direction = GetFireEndLocation() - SpawnLocation;

			Direction.Normalize();
			FMatrix NewMatrix(Direction, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			FRotator SpawnRotation = NewMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefaults* myProjectile = Cast<AProjectileDefaults>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->BulletProjectileMovement->MaxSpeed = WeaponSetting.ProjectileSetting.ProjectileInitSpeed;
					myProjectile->BulletProjectileMovement->InitialSpeed = WeaponSetting.ProjectileSetting.ProjectileInitSpeed;
					myProjectile->BulletInit(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				FHitResult BulletHit;
				TArray<AActor*> IgnoredActor;
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation, UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel3), true, IgnoredActor, EDrawDebugTrace::None, BulletHit, true);
				if (BulletHit.GetActor() && BulletHit.PhysMaterial.IsValid()) {
					EPhysicalSurface tmpSurfaceType = UGameplayStatics::GetSurfaceType(BulletHit);
					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(tmpSurfaceType)) {
						UMaterialInterface* tmpMaterial = WeaponSetting.ProjectileSetting.HitDecals[tmpSurfaceType];

						if (tmpMaterial && BulletHit.GetComponent()) {

							SpawnHitDecal_Multicast(tmpMaterial, BulletHit.GetComponent(), BulletHit.ImpactPoint, BulletHit.ImpactNormal.Rotation());
						}
					}

					if (WeaponSetting.ProjectileSetting.HitFX.Contains(tmpSurfaceType)) {
						UParticleSystem* tmpParticle = WeaponSetting.ProjectileSetting.HitFX[tmpSurfaceType];
						if (tmpParticle) {
							SpawnHitEmitter_Multicast(tmpParticle, FTransform(BulletHit.ImpactNormal.Rotation(), BulletHit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound) {
						SpawnSound_Multicast(WeaponSetting.ProjectileSetting.HitSound, BulletHit.ImpactPoint);
					}
					
					APlayerStates::AddEffectBySurfaceType(BulletHit.GetActor(), ProjectileInfo.Effect, tmpSurfaceType);
					UGameplayStatics::ApplyPointDamage(BulletHit.GetActor(), WeaponSetting.WeaponDamage, BulletHit.ImpactPoint, BulletHit, GetInstigatorController(), this, nullptr);

				}
			}
		}
		if (WeaponSetting.FireSound) {
			SpawnSound_Multicast(WeaponSetting.FireSound, ShootLocation->GetComponentLocation());
		}
		if (WeaponSetting.FireParticles) {

			SpawnEmitterAttached_Multicast(WeaponSetting.FireParticles, ShootLocation);
		}
		if (SleeveLocation && WeaponSetting.ShellBulletMesh.DropMesh) {
			if (WeaponSetting.ShellBulletMesh.DropMeshTime < 0.0f) {
				InitDropMesh_Server(WeaponSetting.ShellBulletMesh.DropMesh, SleeveLocation, WeaponSetting.ShellBulletMesh.DropMeshLifeTime, WeaponSetting.ShellBulletMesh.DropMeshImpulse, WeaponSetting.ShellBulletMesh.DropMeshImpulseDispersion, WeaponSetting.ShellBulletMesh.DropMeshMass);
			}
			else {
				DropShellFlag = true;
				DropShellTimer = WeaponSetting.ShellBulletMesh.DropMeshTime;
			}
		}

		OnWeaponFireStart_Delegate_Multicast(WeaponSetting.PlayerFireAnimation, WeaponSetting.PlayerFireAimAnimation);
	}
	if (GetWeaponRound() <= 0) {
		UInventorySystemComponent* Inventory = Cast<UInventorySystemComponent>(GetOwner()->GetComponentByClass(UInventorySystemComponent::StaticClass()));
		Inventory->UpdateWidgetsAmmo();
		if (!IsReloading && CanWeaponReload())
		{
			WeaponReloadStart_Server();
		}
	}
}

void AWeaponDefaults::UpdateStateWeapon_Server_Implementation(EMovementState NewMovementState)
{
	CanFire = true;
	//ToDo Dispersion
	switch (NewMovementState) {
	case EMovementState::Aim_state:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_DispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_DispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_DispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_DispersionReduction;
		ShouldReduceDispersion = true;
		break;
	case EMovementState::Walk_state:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_DispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_DispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_DispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_DispersionReduction;
		ShouldReduceDispersion = true;
		break;
	case EMovementState::Run_state:
		CanFire = false;
		SetWeaponStateFire_Server(false);
		ShouldReduceDispersion = false;
		break;
	default:
		break;
	}
}

void AWeaponDefaults::ChangeDispersion()
{
}

int32 AWeaponDefaults::GetWeaponRound()
{
	return AddWeaponSetting.Round;
}

int8 AWeaponDefaults::GetAvailableAmmoFromInventory()
{
	int32 result = WeaponSetting.MaxRounds;
	if (GetOwner()) {
		UInventorySystemComponent* Inventory = Cast<UInventorySystemComponent>(GetOwner()->GetComponentByClass(UInventorySystemComponent::StaticClass()));
		if (Inventory) {
			if (!Inventory->CheckAmmoForWeapon(Inventory->CurrentIndex, result)) {
				// �� ����� ��� ��� ������ ����, �� ��� ��������.. ������ ��� ����� � �� ����� � �����.
			}
		}
	}
	return result;
}

void AWeaponDefaults::WeaponReloadStart_Server_Implementation()
{
	IsReloading = true;
	SpawnSound_Multicast(WeaponSetting.ReloadWeapon, ShootLocation->GetComponentLocation());
	ReloadTimer = WeaponSetting.ReloadTime;
	DoOnceReloadEndSound = true;

	if (WeaponSetting.PlayerReloadAnimation) {
		OnWeaponReloadStart_Delegate_Multicast(WeaponSetting.PlayerReloadAnimation);

		if (SkeletalMeshWeapon) {
			if (WeaponSetting.WeaponReloadAnimation) {
				PlayAnimWeapon_Multicast(WeaponSetting.WeaponReloadAnimation);
			}
		}
	}

	if (MagazineLocation && WeaponSetting.ClipDropMesh.DropMesh) {
		if (WeaponSetting.ClipDropMesh.DropMeshTime < 0.0f) {
			InitDropMesh_Server(WeaponSetting.ClipDropMesh.DropMesh, MagazineLocation, WeaponSetting.ClipDropMesh.DropMeshLifeTime, WeaponSetting.ClipDropMesh.DropMeshImpulse, WeaponSetting.ClipDropMesh.DropMeshImpulseDispersion, WeaponSetting.ClipDropMesh.DropMeshMass);
		}
		else {
			DropClipFlag = true;
			DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
		}
	}
}

void AWeaponDefaults::WeaponReloadEnd_Server_Implementation()
{
	IsReloading = false;

	int8 AvailableAmmo = GetAvailableAmmoFromInventory();
	int8 NeedTake;
	int8 NeedToReload = WeaponSetting.MaxRounds - AddWeaponSetting.Round;
	if (NeedToReload > AvailableAmmo) {
		AddWeaponSetting.Round = AvailableAmmo;
		NeedTake = AvailableAmmo;
	}
	else {
		AddWeaponSetting.Round = AddWeaponSetting.Round + NeedToReload;
		NeedTake = NeedToReload;
	}

	OnWeaponReloadEnd_Delegate_Multicast(true, -NeedTake);

	UInventorySystemComponent* Inventory = Cast<UInventorySystemComponent>(GetOwner()->GetComponentByClass(UInventorySystemComponent::StaticClass()));
	Inventory->UpdateWidgetsAmmo();
}

void AWeaponDefaults::CancelReloading_Server_Implementation()
{
	IsReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance()) {
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd_Delegate_Multicast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefaults::CanWeaponReload()
{
	bool result = true;
	if (GetOwner()) {
		UInventorySystemComponent* Inventory = Cast<UInventorySystemComponent>(GetOwner()->GetComponentByClass(UInventorySystemComponent::StaticClass()));
		if (Inventory) {
			int32 AvailableAmmo = 0;
			if (!Inventory->CheckAmmoForWeapon(Inventory->CurrentIndex, AvailableAmmo)) {
				result = false;
			}
		}
	}
	return result;
}

int8 AWeaponDefaults::GetNumberProjectileByShot()
{
	return WeaponSetting.RoundPerShot;
}

FVector AWeaponDefaults::GetFireEndLocation() const
{
	bool IsShootDirection = false;
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	FVector EndLocation = FVector(0.0f);
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic) {
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
	}
	else {
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
	}

	return EndLocation;
}

FVector AWeaponDefaults::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

float AWeaponDefaults::GetCurrentDispersion() const
{
	float result = CurrentDispersion;
	return result;
}

void AWeaponDefaults::DispersionTick(float DeltaTime)
{
	if (!IsReloading) {
		if (!WeaponFiring) {
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - (CurrentDispersionReduction * DeltaTime);
			}
			else {

				CurrentDispersion = CurrentDispersion + (CurrentDispersionReduction * DeltaTime);
			}
		}
		if (CurrentDispersion < CurrentDispersionMin) {
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax) {
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
}

void AWeaponDefaults::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

void AWeaponDefaults::PlayAnimWeapon_Multicast_Implementation(UAnimationAsset* AnimWeapon)
{
	SkeletalMeshWeapon->PlayAnimation(AnimWeapon, false);
}


void AWeaponDefaults::ChangeWeaponState_Server_Implementation(FVector NewShootEndLocation, bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefaults::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefaults, AddWeaponSetting);
	//DOREPLIFETIME(AWeaponDefaults, CurrentWeapon);
}

void AWeaponDefaults::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* NewMaterial, USceneComponent* OtherComp, FVector HitLocation, FRotator HitRotation)
{
	UDecalComponent* Decal = UGameplayStatics::SpawnDecalAttached(NewMaterial, FVector(100.0f, 5.0f, 5.0f), OtherComp, NAME_None, HitLocation, HitRotation, EAttachLocation::KeepWorldPosition, 10.0f);
	Decal->FadeScreenSize = 0.0f;
}

void AWeaponDefaults::SpawnHitEmitter_Multicast_Implementation(UParticleSystem* NewEmitter, FTransform HitTransform)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), NewEmitter, HitTransform);
}

void AWeaponDefaults::SpawnSound_Multicast_Implementation(USoundBase* NewSound, FVector HitLocation)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), NewSound, HitLocation);
}

void AWeaponDefaults::SpawnEmitterAttached_Multicast_Implementation(UParticleSystem* NewEmitter, UArrowComponent* Component)
{
	UGameplayStatics::SpawnEmitterAttached(NewEmitter, Component);
}