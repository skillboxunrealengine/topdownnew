// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefaults.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AProjectileDefaults::AProjectileDefaults()
{

	bReplicates = true;

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(3.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	BulletCollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BulletCollisionSphere->SetCollisionProfileName(TEXT("Bullet"));

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 2000.0f;
	BulletProjectileMovement->MaxSpeed = 2000.0f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = false;

	BulletProjectileMovement->ProjectileGravityScale = 0.1f;
}

// Called when the game starts or when spawned
void AProjectileDefaults::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefaults::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefaults::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefaults::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefaults::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefaults::ImpactProjectile()
{
	Destroy();
}

void AProjectileDefaults::BulletInit(FProjectileInfo InitParams)
{
	ProjectileSetting = InitParams;
	if (InitParams.BulletMesh)
	{
		InitMeshProjectile_Multicast(InitParams.BulletMesh);
	}
	else {
		DestroyMeshProjectile_Multicast();
	}

	if (InitParams.BulletFX) {
		InitTemplateProjectile_Multicast(InitParams.BulletFX);
	}
	else {
		DestroyTemplateProjectile_Multicast();
	}

	this->SetLifeSpan(InitParams.ProjectileLifeTime);
}

void AProjectileDefaults::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid()) {
		EPhysicalSurface tmpSurfaceType = UGameplayStatics::GetSurfaceType(Hit);
		if (ProjectileSetting.HitDecals.Contains(tmpSurfaceType)) {
			UMaterialInterface* tmpMaterial = ProjectileSetting.HitDecals[tmpSurfaceType];

			if (tmpMaterial && OtherComp) {
				SpawnHitDecal_Multicast(tmpMaterial, OtherComp, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
			}
		}

		if (ProjectileSetting.HitFX.Contains(tmpSurfaceType)) {
			UParticleSystem* tmpParticle = ProjectileSetting.HitFX[tmpSurfaceType];
			if (tmpParticle) {
				SpawnHitEmitter_Multicast(tmpParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		if (ProjectileSetting.HitSound) {
			SpawnHitSound_Multicast(ProjectileSetting.HitSound, Hit.ImpactPoint);
		}
		APlayerStates::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSetting.Effect, tmpSurfaceType);
		UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetActorLocation(), Hit, GetInstigatorController(), NULL, nullptr);
	}
	ImpactProjectile();
}

void AProjectileDefaults::BulletCollisionSphereBeginOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}

void AProjectileDefaults::BulletCollisionSphereEndOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
}


void AProjectileDefaults::InitMeshProjectile_Multicast_Implementation(UStaticMesh* NewMesh)
{
	BulletMesh->SetStaticMesh(NewMesh);
}

void AProjectileDefaults::DestroyMeshProjectile_Multicast_Implementation()
{
	BulletMesh->DestroyComponent();
}

void AProjectileDefaults::InitTemplateProjectile_Multicast_Implementation(UParticleSystem* NewTemplate)
{
	BulletFX->SetTemplate(NewTemplate);
}

void AProjectileDefaults::DestroyTemplateProjectile_Multicast_Implementation()
{
	BulletFX->DestroyComponent();
}



void AProjectileDefaults::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* NewMaterial, USceneComponent* OtherComp, FVector HitLocation, FRotator HitRotation)
{
	UDecalComponent* Decal = UGameplayStatics::SpawnDecalAttached(NewMaterial, FVector(100.0f, 5.0f, 5.0f), OtherComp, NAME_None, HitLocation, HitRotation, EAttachLocation::KeepWorldPosition, 10.0f);
	Decal->FadeScreenSize = 0.0f;
}

void AProjectileDefaults::SpawnHitEmitter_Multicast_Implementation(UParticleSystem* NewEmitter, FTransform HitTransform)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), NewEmitter, HitTransform);
}

void AProjectileDefaults::SpawnHitSound_Multicast_Implementation(USoundBase* NewSound, FVector HitLocation)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), NewSound, HitLocation);
}
