// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "TPS_Skillbox/PlayerStates.h"
#include "ProjectileDefaults.generated.h"

UCLASS()
class TPS_SKILLBOX_API AProjectileDefaults : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AProjectileDefaults();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	class UParticleSystemComponent* BulletFX = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FCollisionProfileName CollisionProfile;

	FProjectileInfo ProjectileSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ImpactProjectile();

	UFUNCTION(BlueprintCallable)
	void BulletInit(FProjectileInfo InitParams);
	UFUNCTION()
	void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(NetMulticast, Reliable)
	void InitMeshProjectile_Multicast(UStaticMesh* NewMesh);
	UFUNCTION(NetMulticast, Reliable)
	void DestroyMeshProjectile_Multicast();

	UFUNCTION(NetMulticast, Reliable)
	void InitTemplateProjectile_Multicast(UParticleSystem* NewTemplate);
	UFUNCTION(NetMulticast, Reliable)
	void DestroyTemplateProjectile_Multicast();

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* NewMaterial, USceneComponent* OtherComp, FVector HitLocation, FRotator HitRotation);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitEmitter_Multicast(UParticleSystem* NewEmitter, FTransform HitTransform);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* NewSound, FVector HitLocation);


};
