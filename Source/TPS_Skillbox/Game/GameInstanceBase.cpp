// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstanceBase.h"

bool UGameInstanceBase::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
    bool IsInfoFound = false;
    FWeaponInfo* WeaponInfoRow;

    if (WeaponInfoTable) {
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);

        if (WeaponInfoRow)
        {
            IsInfoFound = true;
            OutInfo = *WeaponInfoRow;
        }
    }

    return IsInfoFound;
}

bool UGameInstanceBase::GetDropWeaponSlotByName(FName NameWeapon, FDropItem& OutInfo)
{
    bool IsInfoFound = false;
    FDropItem* DropItemRow;

    if (DropItemTable) {
        DropItemRow = DropItemTable->FindRow<FDropItem>(NameWeapon, "", false);

        if (DropItemRow)
        {
            IsInfoFound = true;
            OutInfo = *DropItemRow;
        }
    }

    return IsInfoFound;
}
