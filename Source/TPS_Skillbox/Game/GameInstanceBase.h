// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS_Skillbox/PlayerStates.h"
#include "Engine/DataTable.h"
#include "TPS_Skillbox/Weapon/WeaponDefaults.h"

#include "GameInstanceBase.generated.h"

UCLASS()
class TPS_SKILLBOX_API UGameInstanceBase : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / INSTANCE SETTINGS")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / INSTANCE SETTINGS")
	UDataTable* DropItemTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropWeaponSlotByName(FName NameWeapon, FDropItem& OutInfo);
};
