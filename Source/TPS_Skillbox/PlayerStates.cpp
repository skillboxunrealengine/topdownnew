// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStates.h"
#include "TPS_Skillbox.h"
#include "TPS_Skillbox/Interface/IGameActor.h"

void APlayerStates::AddEffectBySurfaceType(AActor* TargetEffectActor, TSubclassOf<UStateEffectBase> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TargetEffectActor && AddEffectClass)
	{
		UStateEffectBase* myEffect = Cast<UStateEffectBase>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool IsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !IsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					IsHavePossibleSurface = true;
					bool IsCanAddEffect = false;
					if (!myEffect->IsStakable)
					{
						int8 j = 0;
						TArray<UStateEffectBase*> CurrentEffects;
						IIGameActor* myInterface = Cast<IIGameActor>(TargetEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !IsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									IsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							IsCanAddEffect = true;
						}

					}
					else
					{
						IsCanAddEffect = true;
					}

					if (IsCanAddEffect)
					{

						UStateEffectBase* NewEffect = NewObject<UStateEffectBase>(TargetEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TargetEffectActor);
						}
					}

				}
				i++;
			}
		}

	}
}
